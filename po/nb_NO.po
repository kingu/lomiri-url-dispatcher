# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Canonical Ltd.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-03-03 17:18-0500\n"
"PO-Revision-Date: 2023-01-04 08:06+0000\n"
"Last-Translator: Allan Nordhøy <epost@anotheragency.no>\n"
"Language-Team: Norwegian Bokmål <https://hosted.weblate.org/projects/lomiri/"
"lomiri-url-dispatcher/nb_NO/>\n"
"Language: nb_NO\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.15.1-dev\n"

#: ../data/bad-url.qml:30
msgid "Unrecognized Address"
msgstr ""

#: ../data/bad-url.qml:31
msgid "Ubuntu can't open addresses of type “%1”."
msgstr ""

#: ../data/bad-url.qml:34
msgid "OK"
msgstr "OK"
